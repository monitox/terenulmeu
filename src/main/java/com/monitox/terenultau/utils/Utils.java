package com.monitox.terenultau.utils;

import com.monitox.terenultau.models.UserLoginDetails;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sl0 on 1/17/17.
 */
public class Utils {
    public static UserLoginDetails getUserDetails() {
        return new UserLoginDetails((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }

    public static List<Integer> generateListOfHours(){
        List<Integer> hours = new ArrayList<>();
        for (int i = 1; i < 24; i++) {
            hours.add(i);
        }
        return hours;
    }
}
