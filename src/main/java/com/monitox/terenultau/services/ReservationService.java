package com.monitox.terenultau.services;

import com.monitox.terenultau.domain.Field;
import com.monitox.terenultau.domain.Reservation;
import com.monitox.terenultau.domain.Status;
import com.monitox.terenultau.domain.User;
import com.monitox.terenultau.exceptions.ResourceNotFound;
import com.monitox.terenultau.exceptions.UserNotAllowed;
import com.monitox.terenultau.models.ReservationModel;
import com.monitox.terenultau.models.UserLoginDetails;
import com.monitox.terenultau.repositories.FieldRepository;
import com.monitox.terenultau.repositories.ReservationRepository;
import com.monitox.terenultau.repositories.UserRepository;
import com.monitox.terenultau.utils.Utils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by sl0 on 6/18/17.
 */
@Service
public class ReservationService {
    private final Logger logger = Logger.getLogger(ReservationService.class);

    private final ReservationRepository reservationRepository;
    private final FieldRepository fieldRepository;
    private final UserRepository userRepository;
    private final EmailService emailService;
    private final SmsService smsService;

    @Autowired
    public ReservationService(ReservationRepository reservationRepository,
                              FieldRepository fieldRepository,
                              UserRepository userRepository,
                              EmailService emailService,
                              SmsService smsService) {
        this.reservationRepository = reservationRepository;
        this.fieldRepository = fieldRepository;
        this.userRepository = userRepository;
        this.emailService = emailService;
        this.smsService = smsService;
    }

    public List<Integer> getAvailableHoursForFieldOnDate(long fieldId, LocalDate date) {
        Field field = fieldRepository.findOne(fieldId);
        if (field == null) {
            throw new ResourceNotFound("field");
        }
        List<Integer> hours = Utils.generateListOfHours();
        field.getReservations().stream().
                filter(reservation -> reservation.getDate().equals(date)).
                filter(reservation -> reservation.getCurrentStatus().equals(Status.ACCEPTED)).
                forEach(reservation -> hours.remove(reservation.getHour()));
        return hours;
    }

    public ReservationModel newReservation(Long fieldId, ReservationModel model, UserLoginDetails loggedUser) {
        Field field = fieldRepository.findOne(fieldId);
        if (field == null) {
            throw new ResourceNotFound("field");
        }

        User user = userRepository.findByEmail(loggedUser.getEmail());

        Reservation reservation = new Reservation();

        reservation.setCurrentStatus(Status.PENDING);
        reservation.setDate(LocalDate.parse(model.getDate()));
        reservation.setField(field);
        reservation.setHour(model.getHour());
        reservation.setUser(user);

        reservationRepository.saveAndFlush(reservation);
        return new ReservationModel(reservation);
    }

    public List<ReservationModel> getReservationsForOwner(UserLoginDetails loggedUser) {
        User user = userRepository.findByEmail(loggedUser.getEmail());

        List<ReservationModel> reservations = new ArrayList<>();

        for (Field f :
                user.getFields()) {
            reservations.addAll(f.getReservations().stream().map(ReservationModel::new).collect(Collectors.toList()));
        }

        reservations.sort(Comparator.comparing(ReservationModel::getHour));
        reservations.sort(Comparator.comparing(ReservationModel::getDate));
        reservations.sort(Comparator.comparing(ReservationModel::getStatus));
        return reservations;
    }

    public ReservationModel getReservation(long reservationId) {
        return new ReservationModel(reservationRepository.findOne(reservationId));
    }

    public ReservationModel updateReservations(ReservationModel reservationModel,
                                               UserLoginDetails loggedUser) {
        User owner = userRepository.findByEmail(loggedUser.getEmail());
        Field field = fieldRepository.findOne(reservationModel.getFieldId());

        if (!owner.getFields().contains(field)) {
            throw new UserNotAllowed("User not allowed to edit field.");
        }

        Reservation reservation = reservationRepository.findOne(reservationModel.getId());

        reservation.setDate(LocalDate.parse(reservationModel.getDate()));
        reservation.setField(field);
        reservation.setHour(reservationModel.getHour());
        reservation.setCurrentStatus(reservationModel.getStatus());

        reservationRepository.saveAndFlush(reservation);
        return new ReservationModel(reservation);
    }

    public ReservationModel acceptReservation(ReservationModel reservationModel, UserLoginDetails loggedUser) {
        User owner = userRepository.findByEmail(loggedUser.getEmail());
        Field field = fieldRepository.findOne(reservationModel.getFieldId());

        if (!owner.getFields().contains(field)) {
            throw new UserNotAllowed("User not allowed to edit field.");
        }

        Reservation reservation = reservationRepository.findOne(reservationModel.getId());
        reservation.setCurrentStatus(Status.ACCEPTED);

        field.getReservations().stream().
                filter(reservation1 -> {
                    return reservation1.getDate().isEqual(reservation.getDate())
                            && reservation1.getHour().equals(reservation.getHour())
                            && !reservation1.getId().equals(reservation.getId());
                }).
                forEach(reservation1 -> {
                    reservation1.setCurrentStatus(Status.REJECTED);
                    reservationRepository.saveAndFlush(reservation1);
                });
        reservationRepository.saveAndFlush(reservation);

        User client = reservation.getUser();

        emailService.sendSimpleMessage(client.getEmail(), "Reservation accepted",
                "Your reservation on TerenulTau was accepted for the field "
                        + field.getName() + " on " + reservation.getDate() + " at "
                        + reservation.getHour() + ":00" + "\nFor more info call "
                        + owner.getPhoneNumber() + ".");

        smsService.send("+4" + client.getPhoneNumber(),
                "Your reservation on TerenulTau was accepted for the field "
                        + field.getName() + " on " + reservation.getDate() + " at "
                        + reservation.getHour() + ":00" + "\nFor more info call "
                        + owner.getPhoneNumber() + ".", null);

        return new ReservationModel(reservation);
    }

    public ReservationModel rejectReservation(ReservationModel reservationModel, UserLoginDetails loggedUser) {
        User owner = userRepository.findByEmail(loggedUser.getEmail());
        Field field = fieldRepository.findOne(reservationModel.getFieldId());

        if (!owner.getFields().contains(field)) {
            throw new UserNotAllowed("User not allowed to edit field.");
        }

        Reservation reservation = reservationRepository.findOne(reservationModel.getId());
        reservation.setCurrentStatus(Status.REJECTED);

        reservationRepository.saveAndFlush(reservation);

        User client = reservation.getUser();

        emailService.sendSimpleMessage(client.getEmail(), "Reservation rejected",
                "Your reservation on TerenulTau was accepted for the field "
                        + field.getName() + " on " + reservation.getDate() + " at "
                        + reservation.getHour() + ":00" + "\nFor more info call "
                        + owner.getPhoneNumber() + ".");

        smsService.send("+4" + client.getPhoneNumber(),
                "Your reservation on TerenulTau was rejected for the field "
                        + field.getName() + " on " + reservation.getDate() + " at "
                        + reservation.getHour() + ":00" + "\nFor more info call "
                        + owner.getPhoneNumber() + ".", null);
        
        return new ReservationModel(reservation);
    }

    public List<ReservationModel> getReservationsHistory(UserLoginDetails loggedUser) {
        User currentUser = userRepository.findByEmail(loggedUser.getEmail());

        List<Reservation> reservations = new ArrayList<>(currentUser.getReservations());
        reservations.sort(Comparator.comparing(Reservation::getHour).reversed());
        reservations.sort(Comparator.comparing(Reservation::getDate).reversed());

        return reservations.stream().map(ReservationModel::new).collect(Collectors.toList());
    }
}

