package com.monitox.terenultau.services;

import com.twilio.exception.TwilioException;
import com.twilio.rest.api.v2010.account.MessageCreator;
import com.twilio.type.PhoneNumber;
import com.twilio.http.TwilioRestClient;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by sl0 on 6/19/17.
 */
@Component
public class SmsService {
    private final Logger logger = Logger.getLogger(SmsService.class);
    @Value("${twillo.phone-number}")
    private String phoneNumber;

    private TwilioRestClient restClient;

    @Autowired
    public SmsService(TwilioRestClient restClient) {
        this.restClient = restClient;
    }

    public void send(String to, String body, String mediaUrl) {
        MessageCreator messageCreator = new MessageCreator(
                new PhoneNumber(to),
                new PhoneNumber(phoneNumber),
                body);
//        messageCreator.setMediaUrl(mediaUrl);
        try {
            messageCreator.create(this.restClient);
        } catch (TwilioException e) {
            logger.error(
                    String.format("An exception occurred trying to send a message to %s, exception: %s",
                            to,
                            e.getMessage()));
        }
    }
}
