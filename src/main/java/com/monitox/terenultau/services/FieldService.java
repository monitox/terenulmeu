package com.monitox.terenultau.services;

import com.monitox.terenultau.domain.Field;
import com.monitox.terenultau.domain.User;
import com.monitox.terenultau.exceptions.ResourceAlreadyExists;
import com.monitox.terenultau.exceptions.UserNotAllowed;
import com.monitox.terenultau.models.FieldModel;
import com.monitox.terenultau.models.UserLoginDetails;
import com.monitox.terenultau.repositories.FieldRepository;
import com.monitox.terenultau.repositories.UserRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by sl0 on 6/15/17.
 */
@Service
public class FieldService {
    private final Logger logger = Logger.getLogger(FieldService.class);

    private final FieldRepository fieldRepository;
    private final UserRepository userRepository;

    @Autowired
    public FieldService(FieldRepository fieldRepository,
                        UserRepository userRepository) {
        this.fieldRepository = fieldRepository;
        this.userRepository = userRepository;
    }

    @CacheEvict(value = "getFields", allEntries = true)
    public FieldModel createField(FieldModel model, UserLoginDetails loggedUser) {
        Field field = fieldRepository.findByName(model.getName());
        if (field != null) {
            throw new ResourceAlreadyExists(model.getName());
        }
        User user = userRepository.findByEmail(loggedUser.getEmail());

        field = new Field();
        field.setAddress(model.getAddress());
        field.setDescription(model.getDescription());
        field.setName(model.getName());
        field.setPrice(model.getPrice());
        field.setBase64Image(model.getBase64Image());
        field.setOwner(user);

        fieldRepository.saveAndFlush(field);
        return new FieldModel(field);
    }

    public List<FieldModel> getFieldsForUser(UserLoginDetails loggedUser) {
        User user = userRepository.findByEmail(loggedUser.getEmail());
        return user.getFields().stream().map(FieldModel::new).collect(Collectors.toList());
    }

    public FieldModel getField(Long fieldId, UserLoginDetails loggedUser) {
        User user = userRepository.findByEmail(loggedUser.getEmail());

        return new FieldModel(fieldRepository.findOne(fieldId));

    }

    @CacheEvict(value = "getFields", allEntries = true)
    public FieldModel editField(FieldModel model, UserLoginDetails loggedUser) {
        User user = userRepository.findByEmail(loggedUser.getEmail());

        Optional<Field> f = user.getFields().stream().filter(field -> field.getId().equals(model.getId())).findFirst();
        if (!f.isPresent()) {
            throw new UserNotAllowed("User doesn't have access to field");
        }

        Field field = f.get();
        field.setPrice(model.getPrice());
        field.setName(model.getName());
        field.setDescription(model.getDescription());
        field.setAddress(model.getAddress());
        if (model.getBase64Image() != null) {
            field.setBase64Image(model.getBase64Image());
        }

        fieldRepository.saveAndFlush(field);
        return new FieldModel(field);
    }

    @Cacheable("getFields")
    public List<FieldModel> getAllFields() {
        return fieldRepository.findAll().stream().map(FieldModel::new).collect(Collectors.toList());
    }
}
