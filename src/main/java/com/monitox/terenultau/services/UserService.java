package com.monitox.terenultau.services;


import com.monitox.terenultau.domain.Role;
import com.monitox.terenultau.domain.User;
import com.monitox.terenultau.exceptions.ResourceAlreadyExists;
import com.monitox.terenultau.exceptions.ResourceNotFound;
import com.monitox.terenultau.models.UserModel;
import com.monitox.terenultau.repositories.UserRepository;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by sl0 on 11/16/16.
 */
@Service
public class UserService {
    private final Logger logger = Logger.getLogger(UserService.class);

    private final UserRepository userRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;


    @Autowired
    public UserService(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }


    public UserModel register(UserModel userModel) {
        User user = userRepository.findByEmail(userModel.getEmail());
        if (user != null){
            logger.error("User " + userModel.getEmail() + " already exists!");
            throw new ResourceAlreadyExists(userModel.getEmail());
        }
        User newUser = new User();
        newUser.setFirstName(userModel.getFirstName());
        newUser.setLastName(userModel.getLastName());
        newUser.setPhoneNumber(userModel.getPhoneNumber());
        newUser.setPassword(bCryptPasswordEncoder.encode(userModel.getPassword()));
        newUser.setEmail(userModel.getEmail());
        newUser.setRole(Role.USER);
        newUser = userRepository.save(newUser);
        userRepository.flush();
        return new UserModel(newUser);
    }

    public List<UserModel> getUsers() {
        return userRepository.findAll().stream().map(UserModel::new).collect(Collectors.toList());
    }

    public UserModel getUser(Long userId) {

        User user =  userRepository.findOne(userId);
        if (user == null){
            throw new ResourceNotFound("user");
        }
        return new UserModel(user);
    }

    public UserModel editUser(UserModel userModel) {
        User user =  userRepository.findOne(userModel.getId());
        if (user == null){
            throw new ResourceNotFound("user");
        }
        if (!StringUtils.isBlank(userModel.getPassword())){
            user.setPassword(bCryptPasswordEncoder.encode(userModel.getPassword()));
        }
        user.setRole(Role.valueOf(userModel.getRole()));
        user.setEmail(userModel.getEmail());
        user.setFirstName(userModel.getFirstName());
        user.setLastName(userModel.getLastName());
        user.setPhoneNumber(userModel.getPhoneNumber());

        userRepository.saveAndFlush(user);

        return userModel;
    }

    public UserModel createUser(UserModel userModel) {
        User user = userRepository.findByEmail(userModel.getEmail());
        if (user != null){
            logger.error("User " + userModel.getEmail() + " already exists!");
            throw new ResourceAlreadyExists(userModel.getEmail());
        }
        User newUser = new User();
        newUser.setFirstName(userModel.getFirstName());
        newUser.setLastName(userModel.getLastName());
        newUser.setPhoneNumber(userModel.getPhoneNumber());
        newUser.setPassword(bCryptPasswordEncoder.encode(userModel.getPassword()));
        newUser.setEmail(userModel.getEmail());
        newUser.setRole(Role.valueOf(userModel.getRole()));
        newUser = userRepository.save(newUser);
        userRepository.flush();
        return new UserModel(newUser);
    }
}
