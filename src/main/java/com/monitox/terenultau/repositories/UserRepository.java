package com.monitox.terenultau.repositories;

import com.monitox.terenultau.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by sl0 on 11/2/16.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
//    @Query(value = "select role from terenultau.role inner join terenultau.user_group on terenultau.role.id = terenultau.user_group.role_id i" +
//            "nner join terenultau.user on terenultau.user.id = terenultau.user_group.user_id where terenultau.user.username = :username",
//            nativeQuery = true)
//    List<String> findRolesByUser(@Param("username")String username);

    User findByEmail(String email);
}
