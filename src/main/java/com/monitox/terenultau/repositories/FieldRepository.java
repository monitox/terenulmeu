package com.monitox.terenultau.repositories;

import com.monitox.terenultau.domain.Field;
import com.monitox.terenultau.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by sl0 on 6/15/17.
 */
@Repository
public interface FieldRepository extends JpaRepository<Field, Long>  {
    Field findByName(String name);
}
