package com.monitox.terenultau.repositories;

import com.monitox.terenultau.domain.Field;
import com.monitox.terenultau.domain.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by sl0 on 6/18/17.
 */
@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {
}

