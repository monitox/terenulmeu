package com.monitox.terenultau.models;

import com.monitox.terenultau.domain.Reservation;
import com.monitox.terenultau.domain.Status;

import java.time.format.DateTimeFormatter;

/**
 * Created by sl0 on 6/18/17.
 */
public class ReservationModel {
    private Long id;
    private Long fieldId;
    private String date;
    private Integer hour;
    private Status status;
    private String fieldName;
    private String clientName;

    public ReservationModel(Reservation reservation) {
        id = reservation.getId();
        fieldId = reservation.getField().getId();
        date = reservation.getDate().format(DateTimeFormatter.ISO_DATE);
        hour = reservation.getHour();
        status = reservation.getCurrentStatus();
        fieldName = reservation.getField().getName();
        clientName = reservation.getUser().getName();
    }

    public ReservationModel() {
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFieldId() {
        return fieldId;
    }

    public void setFieldId(Long fieldId) {
        this.fieldId = fieldId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
