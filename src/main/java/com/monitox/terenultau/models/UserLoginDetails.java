package com.monitox.terenultau.models;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.io.Serializable;
import java.util.Collection;

/**
 * Created by sl0 on 11/16/16.
 */
public class UserLoginDetails implements Serializable {
    private String email;
    private String password;

    public Collection<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    private Collection<GrantedAuthority> authorities;

    public UserLoginDetails(User principal) {
        email = principal.getUsername();
        password = principal.getPassword();
        authorities = principal.getAuthorities();
    }

    public UserLoginDetails() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
