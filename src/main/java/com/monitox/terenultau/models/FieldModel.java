package com.monitox.terenultau.models;

import com.monitox.terenultau.domain.Field;

import java.io.Serializable;

/**
 * Created by sl0 on 6/15/17.
 */
public class FieldModel implements Serializable {
    private long id;
    private String name;
    private String address;
    private String phoneNumber;
    private String description;
    private Long price;
    private String ownerName;
    private long ownerId;
    private String base64Image;

    public FieldModel() {

    }

    public FieldModel(Field field) {
        this.id = field.getId();
        this.name = field.getName();
        this.address = field.getAddress();
        this.phoneNumber = field.getOwner().getPhoneNumber();
        this.description = field.getDescription();
        this.price = field.getPrice();
        this.ownerName = field.getOwner().getFirstName() + " " + field.getOwner().getLastName();
        this.ownerId = field.getOwner().getId();
        this.base64Image = field.getBase64Image();
    }

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }
}
