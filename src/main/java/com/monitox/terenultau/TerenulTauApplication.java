package com.monitox.terenultau;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.monitox.terenultau"})
public class TerenulTauApplication {
    public static void main(String[] args) {
        SpringApplication.run(TerenulTauApplication.class, args);
    }

    //	@Bean
//	CommandLineRunner init(UserRepository userRepository){
//		return (evt) -> Arrays.asList(
//				"jhoeller,dsyer,pwebb,ogierke,rwinch,mfisher,mpollack,jlong".split(","))
//				.forEach(
//						a -> {
//							User account = userRepository.save(new User(a,
//									"password", "ADMIN"));
//						});
//	}
//    @Bean
//    public WebMvcConfigurer corsConfigurer() {
//        return new WebMvcConfigurerAdapter() {
//            @Override
//            public void addCorsMappings(CorsRegistry registry) {
//                registry.addMapping("*").allowedOrigins("*");
//            }
//        };
//    }
}
