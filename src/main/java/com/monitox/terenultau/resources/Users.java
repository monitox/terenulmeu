package com.monitox.terenultau.resources;

import com.monitox.terenultau.exceptions.ResourceNotFound;
import com.monitox.terenultau.models.Response;
import com.monitox.terenultau.models.UserLoginDetails;
import com.monitox.terenultau.models.UserModel;
import com.monitox.terenultau.services.UserService;
import com.monitox.terenultau.utils.Utils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

/**
 * Created by sl0 on 5/1/17.
 */
@RestController
@RequestMapping("/api/users")
public class Users {
    private final UserService userService;
    private final Logger logger = Logger.getLogger(Users.class);


    @Autowired
    public Users(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(method = RequestMethod.POST, path = "/register")
    ResponseEntity<?> register(@RequestBody UserModel userModel) {
        logger.info("Registering user '" + userModel + "'.");
        UserModel user = userService.register(userModel);
        logger.info("Successfully registered user '" + user + "'.");

        return new ResponseEntity<>(new Response(true), new HttpHeaders(), HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @RequestMapping(method = RequestMethod.GET)
    ResponseEntity<?> getUsers() {
        UserLoginDetails loggedUser = Utils.getUserDetails();
        List<UserModel> userModels = userService.getUsers();

        return new ResponseEntity<>(userModels, new HttpHeaders(), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @RequestMapping(method = RequestMethod.GET, path = "/user/{userId}")
    ResponseEntity<?> getUser(@PathVariable("userId") Long userId) {
        if(userId == null){
            throw new ResourceNotFound("with empty userId");
        }

        UserLoginDetails loggedUser = Utils.getUserDetails();

        UserModel userModel = userService.getUser(userId);
        return new ResponseEntity<>(userModel, new HttpHeaders(), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @RequestMapping(method = RequestMethod.PUT, path = "/user/{userId}")
    ResponseEntity<?> editUser(@PathVariable("userId") Long userId,
                               @RequestBody UserModel userModel) {
        if(userId == null){
            throw new ResourceNotFound("with empty userId");
        }

        UserLoginDetails loggedUser = Utils.getUserDetails();

        UserModel newUserModel = userService.editUser(userModel);
        return new ResponseEntity<>(userModel, new HttpHeaders(), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<?> createUser(@RequestBody UserModel userModel) {
        logger.info("Registering user '" + userModel + "'.");
        UserModel user = userService.createUser(userModel);
        logger.info("Successfully registered user '" + user + "'.");

        return new ResponseEntity<>(new Response(true), new HttpHeaders(), HttpStatus.CREATED);
    }
}
