package com.monitox.terenultau.resources;

import com.monitox.terenultau.exceptions.BadLogin;
import com.monitox.terenultau.models.UserLoginDetails;
import com.monitox.terenultau.security.JwtAuthenticationResponse;
import com.monitox.terenultau.security.JwtTokenUtil;
import com.monitox.terenultau.services.LoginService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by sl0 on 11/17/16.
 */

@RestController
@RequestMapping("/api/login")
public class Login {
    Logger logger = Logger.getLogger(Login.class);


    @Value("${jwt.header}")
    private String tokenHeader;

    private final AuthenticationManager authenticationManager;

    private final JwtTokenUtil jwtTokenUtil;

    private final UserDetailsService userDetailsService;

    private final
    LoginService loginService;

    @Autowired
    public Login(AuthenticationManager authenticationManager, JwtTokenUtil jwtTokenUtil, UserDetailsService userDetailsService, LoginService loginService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userDetailsService = userDetailsService;
        this.loginService = loginService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<JwtAuthenticationResponse> login(@RequestBody UserLoginDetails user) {
        boolean auth = loginService.logIn(user);
        if (auth) {

            // Perform the auth
            final Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            user.getEmail(),
                            user.getPassword()
                    )
            );

            final UserDetails userDetails = userDetailsService.loadUserByUsername(user.getEmail());
            final String token = jwtTokenUtil.generateToken(userDetails);

            // Return the token
            return ResponseEntity.ok(new JwtAuthenticationResponse(token,
                    userDetails.getAuthorities().toArray()[0].toString(), userDetails.getUsername()));

        }
        throw new BadLogin("Bad Login");

    }
}
