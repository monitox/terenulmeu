package com.monitox.terenultau.resources;

import com.monitox.terenultau.models.ReservationModel;
import com.monitox.terenultau.models.UserLoginDetails;
import com.monitox.terenultau.services.ReservationService;
import com.monitox.terenultau.utils.Utils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by sl0 on 6/18/17.
 */
@RestController
@RequestMapping("/api/reservations")
public class Reservations {
    private final ReservationService reservationService;
    private final Logger logger = Logger.getLogger(Reservations.class);

    @Autowired
    public Reservations(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER', 'USER')")
    @RequestMapping(method = RequestMethod.GET, path = "/availableHours/{fieldId}")
    ResponseEntity<?> getAvailableHours(@PathVariable("fieldId") Long fieldId,
                                        @RequestParam(value = "date", required = true) String date) {
        UserLoginDetails loggedUser = Utils.getUserDetails();
        List<Integer> hours = reservationService.getAvailableHoursForFieldOnDate(fieldId, LocalDate.parse(date));

        return new ResponseEntity<>(hours, new HttpHeaders(), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER', 'USER')")
    @RequestMapping(method = RequestMethod.POST, path = "/newReservation/{fieldId}")
    ResponseEntity<?> newReservation(@PathVariable("fieldId") Long fieldId,
                                     @RequestBody ReservationModel model) {
        UserLoginDetails loggedUser = Utils.getUserDetails();

        model = reservationService.newReservation(fieldId, model, loggedUser);

        return new ResponseEntity<>(model, new HttpHeaders(), HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER')")
    @RequestMapping(method = RequestMethod.GET, path = "/ownerReservations")
    ResponseEntity<?> gerReservationsByOwner() {
        UserLoginDetails loggedUser = Utils.getUserDetails();

        List<ReservationModel> reservations = reservationService.getReservationsForOwner(loggedUser);

        return new ResponseEntity<>(reservations, new HttpHeaders(), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER', 'USER')")
    @RequestMapping(method = RequestMethod.GET, path = "/history")
    ResponseEntity<?> gerReservationsHistory() {
        UserLoginDetails loggedUser = Utils.getUserDetails();

        List<ReservationModel> reservations = reservationService.getReservationsHistory(loggedUser);

        return new ResponseEntity<>(reservations, new HttpHeaders(), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER', 'USER')")
    @RequestMapping(method = RequestMethod.GET, path = "/{reservationId}")
    ResponseEntity<?> getReservation(@PathVariable long reservationId) {
        UserLoginDetails loggedUser = Utils.getUserDetails();
        ReservationModel model = reservationService.getReservation(reservationId);
        return new ResponseEntity<>(model, new HttpHeaders(), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER')")
    @RequestMapping(method = RequestMethod.PUT, path = "/{reservationId}")
    ResponseEntity<?> updateReservation(@PathVariable long reservationId,
                                        @RequestBody ReservationModel reservationModel) {
        UserLoginDetails loggedUser = Utils.getUserDetails();
        ReservationModel model = reservationService.updateReservations(reservationModel, loggedUser);
        return new ResponseEntity<>(model, new HttpHeaders(), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER')")
    @RequestMapping(method = RequestMethod.POST, path = "/accept/{reservationId}")
    ResponseEntity<?> acceptReservation(@PathVariable long reservationId,
                                        @RequestBody ReservationModel reservationModel) {
        UserLoginDetails loggedUser = Utils.getUserDetails();
        ReservationModel model = reservationService.acceptReservation(reservationModel, loggedUser);
        return new ResponseEntity<>(model, new HttpHeaders(), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER')")
    @RequestMapping(method = RequestMethod.POST, path = "/reject/{reservationId}")
    ResponseEntity<?> rejectReservation(@PathVariable long reservationId,
                                        @RequestBody ReservationModel reservationModel) {
        UserLoginDetails loggedUser = Utils.getUserDetails();
        ReservationModel model = reservationService.rejectReservation(reservationModel, loggedUser);
        return new ResponseEntity<>(model, new HttpHeaders(), HttpStatus.OK);
    }
}