package com.monitox.terenultau.resources;

import com.monitox.terenultau.exceptions.ResourceNotFound;
import com.monitox.terenultau.models.FieldModel;
import com.monitox.terenultau.models.Response;
import com.monitox.terenultau.models.UserLoginDetails;
import com.monitox.terenultau.models.UserModel;
import com.monitox.terenultau.services.FieldService;
import com.monitox.terenultau.utils.Utils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by sl0 on 6/15/17.
 */
@RestController
@RequestMapping("/api/fields")
public class Fields {
    private final FieldService fieldService;
    private final Logger logger = Logger.getLogger(Fields.class);


    @Autowired
    public Fields(FieldService fieldService) {
        this.fieldService = fieldService;
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER')")
    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<?> createField(@RequestBody FieldModel fieldModel) {
        UserLoginDetails loggedUser = Utils.getUserDetails();

        FieldModel field = fieldService.createField(fieldModel, loggedUser);

        return new ResponseEntity<>(new Response(true), new HttpHeaders(), HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER', 'USER')")
    @RequestMapping(method = RequestMethod.GET)
    ResponseEntity<?> getFields() {
        UserLoginDetails loggedUser = Utils.getUserDetails();
        List<FieldModel> fieldModels = fieldService.getAllFields();

        return new ResponseEntity<>(fieldModels, new HttpHeaders(), HttpStatus.CREATED);

    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER')")
    @RequestMapping(method = RequestMethod.GET, path = "/myFields")
    ResponseEntity<?> getMyFields() {
        UserLoginDetails loggedUser = Utils.getUserDetails();
        List<FieldModel> fieldModels = fieldService.getFieldsForUser(loggedUser);

        return new ResponseEntity<>(fieldModels, new HttpHeaders(), HttpStatus.CREATED);

    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER', 'USER')")
    @RequestMapping(method = RequestMethod.GET, path = "/field/{fieldId}")
    ResponseEntity<?> getField(@PathVariable("fieldId") Long fieldId) {
        if(fieldId == null){
            throw new ResourceNotFound("with empty field ID");
        }

        UserLoginDetails loggedUser = Utils.getUserDetails();

        FieldModel fieldModel = fieldService.getField(fieldId, loggedUser);
        return new ResponseEntity<>(fieldModel, new HttpHeaders(), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER')")
    @RequestMapping(method = RequestMethod.PUT, path = "/field/{fieldId}")
    ResponseEntity<?> updateField(@PathVariable("fieldId") Long fieldId,
                                  @RequestBody FieldModel model) {
        if(fieldId == null){
            throw new ResourceNotFound("with empty field ID");
        }

        UserLoginDetails loggedUser = Utils.getUserDetails();

        FieldModel fieldModel = fieldService.editField(model, loggedUser);

        return new ResponseEntity<>(fieldModel, new HttpHeaders(), HttpStatus.OK);
    }
}

