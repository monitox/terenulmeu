package com.monitox.terenultau.domain;

/**
 * Created by sl0 on 6/18/17.
 */
public enum Status {
    PENDING, ACCEPTED, REJECTED
}
