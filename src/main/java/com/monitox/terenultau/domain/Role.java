package com.monitox.terenultau.domain;

/**
 * Created by sl0 on 11/24/16.
 */
public enum Role {
    USER, OWNER ,ADMIN;

    public static String[] toStringArray() {
        String[] roles = new String[Role.values().length];
        int k = 0;
        for (Role r :
                Role.values()) {
            roles[k] = r.toString();
            k+=1;
        }
        return roles;
    }
}
