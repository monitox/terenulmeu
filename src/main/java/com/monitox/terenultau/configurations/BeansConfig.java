package com.monitox.terenultau.configurations;

import com.twilio.http.TwilioRestClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Created by sl0 on 1/17/17.
 */
@Configuration

public class BeansConfig {
    @Value("${twillo.account-sid}")
    private String twillioAccount;
    @Value("${twillo.auth-key}")
    private String key;
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
    @Bean
    public TwilioRestClient twilioRestClient() {
        return new TwilioRestClient.Builder(twillioAccount, key).build();
    }

}
